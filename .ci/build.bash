#!/bin/bash
set -e
set -o xtrace

whoami
env | sort

# Build 
mkdir -p build && cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local ..
make -j`nproc`
make package
